#!/bin/bash

set -x

# Install dependencies
sudo apt install ruby-guestfs

# Install the scripts to ~/bin

set -e

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

mkdir -p ~/bin

for f in "${DIR}/bin/"*; do
  sudo ln -sf "${f}" /usr/local/bin/
done
